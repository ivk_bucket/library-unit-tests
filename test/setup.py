import os
from setuptools import setup, find_packages

install_requires=[ "nose",
                   "nose-parameterized",
                   "nose-ignore-docstring",
                   "texttable",
                   ]

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

packages = find_packages(".")
setup(name="os_path_join_tests",
    version="0.0.1",
    packages = packages,
    package_data = {
        '': ['*.sh', '*.ini', '*.pem', '*.txt'],
        'configs': ['*.yaml'] },
    install_requires=install_requires,

    classifiers=[
        "Development Status :: 1 - Planning",
        "Environment :: Other",
        "License :: Other",
        "Operating System :: Unix",
        "Programming Language :: Python :: 2.7",
        "Topic :: Tests :: Unit test",
        ],
)
