#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import unittest
from nose_parameterized import parameterized

class OsPathJoinTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @parameterized.expand([
        ('one character path', ['a'], 'a'),
        ('single path empty string', [''], ''),
        ('single path slash', ['/'], '/'),
        ('newline charackter', ['\n'], '\n'),
        ('tab charackter', ['\t'], '\t'),
        ('single path relative big length', ['a'*1024*1024], 'a'*1024*1024),
        ('single path absolute big length', ['/'+'a'*1024*1024], '/'+'a'*1024*1024),
        ('single path None', [None], None),
        ('single path int', [53], 53),
        ('single path negative', [-10], -10),
        ('single path fract', [3.14], 3.14),
        ('single path bool', [True], True),
        ('single path empty list', [[]], []),
        ('single path list', [['qwerty', 23434]], ['qwerty', 23434]),
        ('single path list of null', [[None]], [None]),
        ('single path empty tuple', [()], ()),
        ('single path tuple', [('qwerty', 23434)], ('qwerty', 23434)),
        ('single path empty dict', [{}], {}),
        ('single path dict', [{'key1': ['qwerty', 23434], 'key2': 123}], {'key1': ['qwerty', 23434], 'key2': 123}),

        ('absolute one argument', ['/home'], '/home'),
        ('relative one arguent', ['folder'], 'folder'),
        ('absolute with relative two arguments', ['/home', 'folder'], '/home/folder'),
        ('relative with absolute two arguments', ['folder', '/home'], '/home'),
        ('absolute with absolute two arguments', ['/first_folder', '/second_folder'], '/second_folder'),
        ('relative with relative two arguments', ['first_folder', 'second_folder'], 'first_folder/second_folder'),

        ('absolute very big with absolute very big two arguments',
                                            ['/'+'a'*1024*1024, '/'+'b'*1024*1024], '/'+'b'*1024*1024),
         # on my Ubuntu NAME_MAX=255 and PATH_MAX=4096, so 1024*1024 characters will be enough considering that python
         # dynamically allocate string size

        ('absolute very big with relative very big two arguments',
                                            ['/'+'a'*1024*1024, 'b'*1024*1024], '/'+'a'*1024*1024+'/'+'b'*1024*1024),

        ('relative very big with absolute very big two arguments',
                                            ['a'*1024*1024, '/'+'b'*1024*1024], '/'+'b'*1024*1024),

        ('relative very big with relative very big two arguments',
                                            ['a'*1024*1024, 'b'*1024*1024], 'a'*1024*1024+'/'+'b'*1024*1024),

        ('lot of relative paths', ['a' for i in range(4096)], 'a/'*4095+'a'),
        ('lot of absolute paths', ['/a' for i in range(4096)], '/a'),
        ('lot of big relative paths', ['a'*256 for i in range(24)], ('a'*256+'/')*23+'a'*256),
        ('lot of big absolute paths', ['/'+'a'*256 for i in range(24)], ('/'+'a'*256)),

        ('such a home path', ['~', 'first_folder/second_folder/'], '~/first_folder/second_folder/'),

        ('win style', ['c:\\folder', '\\dirname'], 'c:\\folder/\\dirname'),
    ])
    def os_path_join_success_test(self, name, params, ethalon_result):
        result = os.path.join(*params)
        self.assertEqual(result, ethalon_result, '')

    @parameterized.expand([
        ('no params', '', "join() takes at least 1 argument (0 given)"),

        ('first argument bool', [True, 'folder'], "'bool' object has no attribute 'endswith'"),
        ('second argument bool', ['folder', True], "'bool' object has no attribute 'startswith'"),
        ('first argument none', [None, 'folder'], "'NoneType' object has no attribute 'endswith'"),
        ('second argument none', ['folder', None], "'NoneType' object has no attribute 'startswith'"),
        ('first argument int', [1234, 'folder'], "'int' object has no attribute 'endswith'"),
        ('second argument int', ['folder', 1234], "'int' object has no attribute 'startswith'"),
        ('first argument negative', [-10, 'folder'], "'int' object has no attribute 'endswith'"),
        ('second argument negative', ['folder', -10], "'int' object has no attribute 'startswith'"),
        ('first argument float', [3.14, 'folder'], "'float' object has no attribute 'endswith'"),
        ('second argument float', ['folder', 3.14], "'float' object has no attribute 'startswith'"),
        ('first argument list', [['qawerty', 12345], 'folder'], "'list' object has no attribute 'endswith'"),
        ('second argument list', ['folder', []], "'list' object has no attribute 'startswith'"),
        ('first argument tuple', [(), 'folder'], "'tuple' object has no attribute 'endswith'"),
        ('second argument tuple', ['folder', (1234, 'qwerty')], "'tuple' object has no attribute 'startswith'"),
        ('first argument dict', [{'qawerty': 12345, 'poiuy': '12345'}, 'folder'], "'dict' object has no attribute 'endswith'"),
        ('second argument dict', ['folder', {}], "'dict' object has no attribute 'startswith'"),
        ('second argument dict', ['folder', {}], "'dict' object has no attribute 'startswith'"),

        ('two arguments wrong last bool', [3.14, True], "'bool' object has no attribute 'startswith'"),
        # etc. other cases depending on needed coverage
    ])
    def os_path_join_wrong_params_test(self, name, wrong_params, error_msg):
        try:
            os.path.join(*wrong_params)
        except Exception, e:
            self.assertEqual(e.message, error_msg)

