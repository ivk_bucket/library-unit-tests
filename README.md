Guide to test development
===========
* python bootstrap.py
* bin/buildout
* Open PyCharm and select root project folder in "Open Directory"
* Open Settings dialog and select "Enable buildout support" in "Buildout Support" item(Set path to bin/buildout file)
* To run all test **bin/nosetests-2.7 test/ -vds --exe**
* To run special suite **bin/nosetests-2.7 test/os_path_join_test.py -vds**
* To run special class**bin/nosetests-2.7 test/os_path_join_test.py:TestOsPathJoin -vds**
* To run special suite and case **bin/nosetests-2.7 test/os_path_join_test.py:TestOsPathJoin.os_path_join_wrong_params_test -vds**
* For more information about tests running see https://nose.readthedocs.org/‎
